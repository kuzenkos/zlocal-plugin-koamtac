import babel from "@rollup/plugin-babel";
import commonjs from "@rollup/plugin-commonjs";
import resolve from "@rollup/plugin-node-resolve";
import pkg from "./package.json";
import { terser } from "rollup-plugin-terser";

const extensions = [".js", ".jsx", ".ts", ".tsx"];

export default {
  input: "./src/KoamTacScannerPlugin.ts",
  output: {
    file: "./www/KoamTacScannerPlugin.js",
    format: "umd",
    name: pkg.name,
    globals: {
      cordova: "cordova",
    },
  },
  external: ["cordova"],
  plugins: [
    resolve({ extensions }),
    commonjs(),
    babel({
      extensions,
      babelHelpers: "inline",
      include: ["src/**/*"],
    }),
    terser()
  ],
};
