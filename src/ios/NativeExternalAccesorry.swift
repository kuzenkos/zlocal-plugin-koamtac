import Foundation
import ExternalAccessory

class KDCScanner: NSObject, EAAccessoryDelegate, StreamDelegate {
    private let _protocolString: String = "com.koamtac.kdc"
    private var _accessory: EAAccessory?
    private var _session: EASession?
    private var _writeData: NSMutableData?
    private var _readData: NSMutableData?
    private var _dataAsString: String?
    private var _dataAsHexString: String?

    static let instance = KDCScanner()

    // MARK: Controller Setup
    func setupController(forAccessory accessory: EAAccessory) {
        _accessory = accessory
    }

    // MARK: Opening & Closing Sessions
    func openSession() -> Bool {
        _accessory?.delegate = self
        _session = EASession(accessory: _accessory!, forProtocol: _protocolString)

        if _session != nil {
            _session?.inputStream?.delegate = self
            _session?.inputStream?.schedule(in: RunLoop.current, forMode: RunLoop.Mode.default)
            _session?.inputStream?.open()

            _session?.outputStream?.delegate = self
            _session?.outputStream?.schedule(in: RunLoop.current, forMode: RunLoop.Mode.default)
            _session?.outputStream?.open()
        } else {
            print("Failed to create session")
        }

        return _session != nil
    }

    func closeSession() {
        _session?.inputStream?.close()
        _session?.inputStream?.remove(from: RunLoop.current, forMode: RunLoop.Mode.default)
        _session?.inputStream?.delegate = nil

        _session?.outputStream?.close()
        _session?.outputStream?.remove(from: RunLoop.current, forMode: RunLoop.Mode.default)
        _session?.outputStream?.delegate = nil

        _session = nil
        _writeData = nil
        _readData = nil
    }

    // MARK: Write & Read Data
    func writeData(data: Data) {
        if _writeData == nil {
            _writeData = NSMutableData()
        }

        _writeData?.append(data)
        self.writeData()
    }

    func readData(bytesToRead: Int) -> Data {
        var data: Data?
        if (_readData?.length)! >= bytesToRead {
            let range = NSMakeRange(0, bytesToRead)
            data = _readData?.subdata(with: range)
            _readData?.replaceBytes(in: range, withBytes: nil, length: 0)
        }

        return data!
    }

    func readBytesAvailable() -> Int {
        return (_readData?.length)!
    }

    // MARK: - Helpers
    func updateReadData() {
        let bufferSize = 128
        var buffer = [UInt8](repeating: 0, count: bufferSize)

        while _session?.inputStream?.hasBytesAvailable == true {
            let bytesRead = _session?.inputStream?.read(&buffer, maxLength: bufferSize)
            if _readData == nil {
                _readData = NSMutableData()
            }
            _readData?.append(buffer, length: bytesRead!)
            _dataAsString = NSString(bytes: buffer, length: bytesRead!, encoding: String.Encoding.utf8.rawValue) as String?
//            _dataAsHexString = NSString(bytes: buffer, length: bytesRead!, encoding: String.Encoding.RawValue)
            _dataAsHexString = _dataAsString // .hexadecimalString()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "BESessionDataReceivedNotification"), object: nil)
        }
    }

    private func writeData() {
        while (_session?.outputStream?.hasSpaceAvailable)! == true && _writeData != nil && (_writeData?.length)! > 0 {
            var buffer = [UInt8](repeating: 0, count: _writeData!.length)
            _writeData?.getBytes(&buffer, length: (_writeData?.length)!)
            let bytesWritten = _session?.outputStream?.write(&buffer, maxLength: _writeData!.length)
            if bytesWritten == -1 {
                print("Write Error")
                return
            } else if bytesWritten! > 0 {
                _writeData?.replaceBytes(in: NSMakeRange(0, bytesWritten!), withBytes: nil, length: 0)
            }
        }
    }

    // MARK: - EAAcessoryDelegate
    func accessoryDidDisconnect(_ accessory: EAAccessory) {
        // Accessory diconnected from iOS, updating accordingly
    }

    // MARK: - NSStreamDelegateEventExtensions
    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        switch eventCode {
        case Stream.Event.openCompleted:
            break
        case Stream.Event.hasBytesAvailable:
            self.updateReadData()
            break
        case Stream.Event.hasSpaceAvailable:
            self.writeData()
            break
        case Stream.Event.errorOccurred:
            break
        case Stream.Event.endEncountered:
            break

        default:
            break
        }
    }
}


//extension String {
//    func dataFromHexString() -> Data? {
//        guard let chars = cString(using: String.Encoding.utf8) else { return nil }
//        let length = count
//        guard let data = NSMutableData(capacity: length/2) else { return nil }
//        var byteChars: [CChar] = [0, 0, 0]
//        var wholeByte: CUnsignedLong = 0
//
//        for i in stride(from: 0, to: length, by: 2) {
//            byteChars[0] = chars[i]
//            byteChars[1] = chars[i + 1]
//            wholeByte = strtoul(byteChars, nil, 16)
//            data.append(&wholeByte, length: 1)
//        }
//        return data as Data
//    }
//
//    init?(hexadecimalString: String) {
//        guard let data = hexadecimalString.dataFromHexString() else {
//            return nil
//        }
//
//        self.init(data: data, encoding: String.Encoding.utf8)
//    }
//
//    func hexadecimalString() -> String? {
//        return data(using: String.Encoding.utf8)?.hexEncodedString()
//    }
//}


//
//extension String.Index{
//    func successor(in string:String)->String.Index{
//        return string.index(after: self)
//    }
//
//    func predecessor(in string:String)->String.Index{
//        return string.index(before: self)
//    }
//
//    func advance(_ offset:Int, `for` string:String)->String.Index{
//        return string.index(self, offsetBy: offset)
//    }
//}
//
//
//extension Data {
//    func hexEncodedString() -> String {
//        return map { String(format: "%02hhx", $0) }.joined()
//    }
//}


import Foundation
import ExternalAccessory

var nc = NotificationCenter.default

@objc(KoamTacScannerPlugin)
class KoamTacScannerPlugin: CDVPlugin {
    private let scanner = KDCScanner.instance
    private var accessoryList: [EAAccessory]?
    var callbackId: String?

    override func pluginInitialize() {
        super.pluginInitialize()

    }

    @objc(init:)
    func _init(command: CDVInvokedUrlCommand) {
        self.commandDelegate.run(inBackground: {
            self.callbackId = command.callbackId

            NotificationCenter.default.addObserver(self, selector: #selector(self.accessoryDidConnect), name: NSNotification.Name.EAAccessoryDidConnect, object: nil)

            NotificationCenter.default.addObserver(self, selector: #selector(self.accessoryDidDisconnect), name: NSNotification.Name.EAAccessoryDidDisconnect, object: nil)

            EAAccessoryManager.shared().registerForLocalNotifications()
            accessoryList = EAAccessoryManager.shared().connectedAccessories

            _ = self.scanner.openSession()

        })
    }


    // MARK: - EAAccessoryNotification Handlers

    @objc
    func accessoryDidConnect(notificaton: NSNotification) {
        let connectedAccessory = notificaton.userInfo![EAAccessoryKey]  as! EAAccessory
        accessoryList?.append(connectedAccessory)

        NSLog("com.koamtac.kdc accessoryDidConnect: %@", connectedAccessory.name)
    }


    @objc
    func accessoryDidDisconnect(notification: NSNotification) {
        let disconnectedAccessory =             notification.userInfo![EAAccessoryKey]
        var disconnectedAccessoryIndex =        0
        for accessory in accessoryList! {
            if (disconnectedAccessory as! EAAccessory).connectionID == accessory.connectionID {
                break
            }
            disconnectedAccessoryIndex += 1
            NSLog("com.koamtac.kdc accessoryDidDisconnect: %@", accessory.name)
        }

    }


    @objc
    func kdcConnectionChanged(notification:NSNotification!) {
        let status = (notification.userInfo?["keyConnectionState"] ?? 0) as! Int64
        var statusText : String {
            switch status {
            case 0: return "NONE"
            case 1: return "LISTEN"
            case 2: return "CONNECTING"
            case 3: return "CONNECTED"
            case 4: return "LOST"
            case 5: return "FAILED"
            case 7: return "INITIALIZING"
            case 8: return "INITIALIZING_FAILED"
            default: return "NONE"
            }
        }
        let message:String! = String(format:"{\"status\": \"%@\", \"code\": \"%d\"}", statusText, status)

//        if kdcReader.isConnected() {
//            kdcReader.enableBluetoothAutoConnect(EnableDisable.ENABLE)
//            kdcReader.enableAutoReconnect(EnableDisable.ENABLE)
//        }

        self.sendPluginResult(message: message, keepCallback: true)
    }
//
//    @objc func kdcDataArrived(notification:NSNotification!) {
//        let kReader:KDCReader! = notification.object as? KDCReader
//        let message:String! = String(format:"{\"scan\":\"%@\"}", kReader.getData())
//
//        self.sendPluginResult(message: message, keepCallback: true)
//    }
//
//    @objc func kdcBarcodeDataArrived(notification:NSNotification!) {
//        let kReader:KDCReader! = notification.object as? KDCReader
//        let message:String! = String(format:"{\"scan\":\"%@\"}", kReader.getBarcodeData())
//
//        self.sendPluginResult(message: message, keepCallback: true)
//    }
//
//    @objc func kdcMSRDataArrived(notification:NSNotification!) {
//        let kReader:KDCReader! = notification.object as? KDCReader
//        let message:String! = String(format:"{\"scan\":\"%@\"}", kReader.getMSRData())
//
//        self.sendPluginResult(message: message, keepCallback: true)
//    }
//
//    @objc func kdcNFCDataArrived(notification:NSNotification!) {
//        let kReader:KDCReader! = notification.object as? KDCReader
//        let message:String! = String(format:"{\"scan\":\"%@\"}", kReader.getNFCUID())
//
//        self.sendPluginResult(message: message, keepCallback: true)
//    }
//
//    @objc func kdcGPSDataArrived(notification:NSNotification!) {
//        let kReader:KDCReader! = notification.object as? KDCReader
//        let message:String! = String(format:"{\"scan\":\"%@\"}", kReader.getGPSData())
//
//        self.sendPluginResult(message: message, keepCallback: true)
//    }
//
//    @objc func didBecomeActive(notification: NSNotification) {
//        if !kdcReader.isConnected() {
//            kdcReader.connect()
//        }
//    }
//
//    @objc func didEnterBackground(notification: NSNotification) {
//        if kdcReader.isConnected() {
//            kdcReader.disconnect()
//        }
//    }

//    @objc func trigger(command:CDVInvokedUrlCommand!) {
//        kdcReader.softwareTrigger()
//    }

    func sendPluginResult(message: String, keepCallback: Bool) {
        let pluginResult = CDVPluginResult(
            status: CDVCommandStatus_OK,
            messageAs: message
        )

        if (keepCallback) {
            pluginResult?.setKeepCallbackAs(keepCallback)
        }

        self.commandDelegate.send(pluginResult, callbackId: self.callbackId)
    }
}
