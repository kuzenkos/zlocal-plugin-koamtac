!function(e,n){"object"==typeof exports&&"undefined"!=typeof module?module.exports=n():"function"==typeof define&&define.amd?define(n):(e="undefined"!=typeof globalThis?globalThis:e||self)["zlocal-plugin-koamtac"]=n()}(this,(function(){"use strict";function e(e,n){for(var t=0;t<n.length;t++){var o=n[t];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(e,o.key,o)}}
/*!
	 * Module dependencies.
	 */
var n=cordova.require("cordova/exec"),t="KoamTacScannerPlugin",o={code:2,message:"Result parse error"};return function(){function r(e,i){!function(e,n){if(!(e instanceof n))throw new TypeError("Cannot call a class as a function")}(this,r),setTimeout((function(){n((function(n){try{n=JSON.parse(n)}catch(e){i(o)}e(n)}),i,t,"init",[])}),10)}var i,a,u;return i=r,(a=[{key:"trigger",value:function(){n((function(){return!0}),(function(){return!0}),t,"trigger",[])}}])&&e(i.prototype,a),u&&e(i,u),r}()}));
