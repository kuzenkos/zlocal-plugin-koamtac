/* global cordova:false */
/* globals window */

/*!
 * Module dependencies.
 */

const exec = cordova.require("cordova/exec");
const PLUGIN_NAME = "KoamTacScannerPlugin";
const ERRORS = {
	CONNECTION_ERROR: { code: 1, message: "KoamTac is Not Connected" },
	PARSE_ERROR: { code: 2, message: "Result parse error" },
};

export default class KoamTacScannerPlugin {
	constructor(successHandler, errorHandler) {
		setTimeout(() => {
			exec((result) => {
				try {
					result = JSON.parse(result);
				} catch (e) {
					errorHandler(ERRORS.PARSE_ERROR);
				}

				successHandler(result);
			}, errorHandler, PLUGIN_NAME, "init", []);
		}, 10);
	}

	trigger() {
		exec(() => true, () => true, PLUGIN_NAME, "trigger", []);
	}
}
