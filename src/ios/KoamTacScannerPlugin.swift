import Foundation

var kdcReader = KDCReader()

@objc(KoamTacScannerPlugin) class KoamTacScannerPlugin: CDVPlugin {
    private var rootView: UIView!
    var callbackId: String?

    override func pluginInitialize() {
        super.pluginInitialize()

        NotificationCenter.default.addObserver(self, selector: #selector(self.kdcConnectionChanged), name: NSNotification.Name.kdcConnectionChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.kdcDataArrived), name: NSNotification.Name.kdcDataArrived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.kdcBarcodeDataArrived), name: NSNotification.Name.kdcBarcodeDataArrived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.kdcMSRDataArrived), name: NSNotification.Name.kdcMSRDataArrived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.kdcNFCDataArrived), name: NSNotification.Name.kdcNFCDataArrived, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.kdcGPSDataArrived), name: NSNotification.Name.kdcGPSDataArrived, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.didEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }

    @objc(init:) func _init(command: CDVInvokedUrlCommand) {
        self.commandDelegate.run(inBackground: {
            self.callbackId = command.callbackId

            if !kdcReader.isConnected() {
                kdcReader.connect()
            }
        })
    }

    private let toastViewWidth: CGFloat = 300
    private let toastViewHeight: CGFloat = 46
    private let toastAnimationDuration: TimeInterval = 0.33
    private let toastAnimationDelay: TimeInterval = 0.1

    private lazy var toastView: UILabel = {
        let frame = CGRect(
            x: UIScreen.main.bounds.size.width / 2 - toastViewWidth / 2,
            y: UIScreen.main.bounds.size.height + 100,
            width: toastViewWidth,
            height: toastViewHeight
        )
        let toastView = UILabel(frame: frame)

        toastView.alpha = 1.0
        toastView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastView.font = .systemFont(ofSize: 14.0, weight: .medium)
        toastView.textAlignment = .center;
        toastView.textColor = UIColor.white
        toastView.clipsToBounds = true

        toastView.layer.cornerRadius = 10;

        return toastView
    }()

    private func showToast(message : String) {
        self.toastView.text = message
        self.webView.superview?.addSubview(self.toastView)

        UIView.animate(withDuration: toastAnimationDuration, delay: toastAnimationDelay, options: .curveEaseOut, animations: {
            self.toastView.frame = CGRect(
                x: self.toastView.frame.origin.x,
                y: self.toastView.frame.origin.y - 200,
                width: self.toastView.frame.size.width,
                height: self.toastView.frame.size.height
            )

            self.webView.superview?.layoutIfNeeded()
        })
    }

    private func hideToast() {
        UIView.animate(withDuration: toastAnimationDuration, delay: toastAnimationDelay, options: .curveEaseOut, animations: {
            self.toastView.frame = CGRect(
                x: self.toastView.frame.origin.x,
                y: self.toastView.frame.origin.y + 200,
                width: self.toastView.frame.size.width,
                height: self.toastView.frame.size.height
            )

            self.webView.superview?.layoutIfNeeded()
        }, completion: {(isCompleted) in
            self.toastView.removeFromSuperview()
        })
    }

    @objc func kdcConnectionChanged(notification:NSNotification!) {
        let status = (notification.userInfo?["keyConnectionState"] ?? 0) as! Int64
        var statusText : String {
            switch status {
            case 0: return "NONE"
            case 1: return "LISTEN"
            case 2: return "CONNECTING"
            case 3: return "CONNECTED"
            case 4: return "LOST"
            case 5: return "FAILED"
            case 7: return "INITIALIZING"
            case 8: return "INITIALIZING_FAILED"
            default: return "NONE"
            }
        }
        let message:String! = String(format:"{\"status\": \"%@\", \"code\": \"%d\"}", statusText, status)

        if (status == 7) {
            self.showToast(message: "Initializing Scanner...");
        } else {
            self.hideToast()
        }

        if kdcReader.isConnected() {
            kdcReader.enableBluetoothAutoConnect(EnableDisable.ENABLE)
            kdcReader.enableAutoReconnect(EnableDisable.ENABLE)
        }

        self.sendPluginResult(message: message, keepCallback: true)
    }

    @objc func kdcDataArrived(notification:NSNotification!) {
        let kReader:KDCReader! = notification.object as? KDCReader
        let message:String! = String(format:"{\"scan\":\"%@\"}", kReader.getData())

        self.sendPluginResult(message: message, keepCallback: true)
    }

    @objc func kdcBarcodeDataArrived(notification:NSNotification!) {
        let kReader:KDCReader! = notification.object as? KDCReader
        let message:String! = String(format:"{\"scan\":\"%@\"}", kReader.getBarcodeData())

        self.sendPluginResult(message: message, keepCallback: true)
    }

    @objc func kdcMSRDataArrived(notification:NSNotification!) {
        let kReader:KDCReader! = notification.object as? KDCReader
        let message:String! = String(format:"{\"scan\":\"%@\"}", kReader.getMSRData())

        self.sendPluginResult(message: message, keepCallback: true)
    }

    @objc func kdcNFCDataArrived(notification:NSNotification!) {
        let kReader:KDCReader! = notification.object as? KDCReader
        let message:String! = String(format:"{\"scan\":\"%@\"}", kReader.getNFCUID())

        self.sendPluginResult(message: message, keepCallback: true)
    }

    @objc func kdcGPSDataArrived(notification:NSNotification!) {
        let kReader:KDCReader! = notification.object as? KDCReader
        let message:String! = String(format:"{\"scan\":\"%@\"}", kReader.getGPSData())

        self.sendPluginResult(message: message, keepCallback: true)
    }

    @objc func didBecomeActive(notification: NSNotification) {
        if !kdcReader.isConnected() {
            kdcReader.connect()
        }
    }

    @objc func didEnterBackground(notification: NSNotification) {
        if kdcReader.isConnected() {
            kdcReader.disconnect()
        }
    }

    @objc func trigger(command:CDVInvokedUrlCommand!) {
        kdcReader.softwareTrigger()
    }

    func sendPluginResult(message: String, keepCallback: Bool) {
        let pluginResult = CDVPluginResult(
            status: CDVCommandStatus_OK,
            messageAs: message
        )

        if (keepCallback) {
            pluginResult?.setKeepCallbackAs(keepCallback)
        }

        self.commandDelegate.send(pluginResult, callbackId: self.callbackId)
    }
}
