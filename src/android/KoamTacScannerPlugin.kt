package com.movingpro

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.util.Log
import koamtac.kdc.sdk.*
import org.apache.cordova.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

@Suppress("unused")
object KoamTacScannerPlugin : CordovaPlugin(), KDCDataReceivedListener, KDCBarcodeDataReceivedListener, KDCGPSDataReceivedListener, KDCMSRDataReceivedListener, KDCNFCDataReceivedListener, KDCConnectionListener {
	private const val LOG_TAG = "KoamTacScannerPlugin"
	private lateinit var context: CallbackContext
	private lateinit var bluetoothAdapter: BluetoothAdapter
	private lateinit var kdcReader: KDCReader

	override fun initialize(cordova: CordovaInterface, webView: CordovaWebView) {
		super.initialize(cordova, webView)
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
		kdcReader = KDCReader(this, this, this, this, this, this, false)
	}

	override fun onReset() = disconnect()
	override fun onDestroy() = disconnect()

	override fun onResume(multitasking: Boolean) {
		checkAndConnect()
		if (kdcReader.IsConnected()) {
			kdcReader.EnableBluetoothAutoConnect(true)
			kdcReader.EnableAutoReconnect(true)
		}
	}

	override fun onPause(multitasking: Boolean) {
		if (kdcReader.IsConnected()) {
			kdcReader.EnableBluetoothAutoConnect(false)
			kdcReader.EnableAutoReconnect(false)
		}
	}

	@Throws(JSONException::class)
	override fun execute(action: String, args: JSONArray, callbackContext: CallbackContext): Boolean {
		context = callbackContext
		try {
			when (action) {
				"init" -> {
					checkAndConnect()
					if (kdcReader.IsConnected()) {
						kdcReader.EnableBluetoothAutoConnect(true)
						kdcReader.EnableAutoReconnect(true)
					}
				}
				"trigger" -> {
					kdcReader.SoftwareTrigger()
				}
				else -> {
					throw JSONException("Method not found")
				}
			}
		} catch (e: JSONException) {
			handleException(e)
			return false
		}
		return true
	}

	// Process and send Near-Field-Communication data events from KDC
	// Only get UID.  NFC Data can have many records, and is difficult to parse.
	// see http://www.kdc100.com/documents/manuals/How_To_Use_Your_KDC_with_NFC_or_RFID.pdf
	// maybe one day use Android NFC classes?
	override fun NFCDataReceived(data: KDCData) = sendScanData(data.GetNFCUID())
	override fun DataReceived(data: KDCData) = sendScanData(data.GetData())
	override fun BarcodeDataReceived(data: KDCData) = sendScanData(data.GetData())
	override fun GPSDataReceived(data: KDCData) = sendScanData(data.GetData())
	override fun MSRDataReceived(data: KDCData) = sendScanData(data.GetData())

	// Handle Connection States of KDC Devices
	override fun ConnectionChanged(device: BluetoothDevice, state: Int) {
		val status = when (state) {
			KDCConstants.CONNECTION_STATE_CONNECTED -> "CONNECTED"
			KDCConstants.CONNECTION_STATE_CONNECTING -> "CONNECTING"
			KDCConstants.CONNECTION_STATE_LOST -> {
				checkAndConnect()
				"LOST"
			}
			KDCConstants.CONNECTION_STATE_FAILED -> "FAILED"
			KDCConstants.CONNECTION_STATE_LISTEN -> "LISTEN"
			else -> "FAILED"
		}

		val response = JSONObject().apply {
			put("status", status)
		}
		sendPluginResult(response, true)
	}

	// Check Bluetooth and Connect to existing device or first available device
	private fun checkAndConnect() {
		if (!bluetoothAdapter.isEnabled) {
			Log.d(LOG_TAG, "Bluetooth Adapter disabled.  Prompting to enable")
			// bluetooth not enabled.  prompt user to enable it
			val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
			cordova.startActivityForResult(this, enableIntent, 1775) // 1775 = REQUEST_CODE_ENABLE_BT;
		} else if (!kdcReader.IsConnected()) {
			Log.d(LOG_TAG, "Attempting to re-connect existing KDC")
			// reconnect to existing device
			kdcReader = KDCReader(this, this, this, this, this, this, false)
		} else {
			Log.d(LOG_TAG, "Using existing connected KDC")
		}
	}

	private fun disconnect() {
		kdcReader.Disconnect()
	}

	// Builds scan message and pluginResult. Sends pluginResult
	private fun sendScanData(message: String) {
		val response = JSONObject().apply {
			put("scan", message)
		}
		sendPluginResult(response, true)
	}

	// Builds scan message and pluginResult. Sends pluginResult
	@Suppress("SameParameterValue")
	private fun sendPluginResult(message: JSONObject, keepCallback: Boolean) {
		val pluginResult = PluginResult(PluginResult.Status.OK, message)
		pluginResult.keepCallback = keepCallback
		context.sendPluginResult(pluginResult)
	}

	/**
	 * Handles an error while executing a plugin API method.
	 * Calls the registered Javascript plugin error handler callback.
	 *
	 * @param errorMsg Error message to pass to the JS error handler
	 */
	private fun handleError(errorMsg: String) {
		try {
			Log.e(LOG_TAG, errorMsg)
			context.error(errorMsg)
		} catch (e: Exception) {
			Log.e(LOG_TAG, e.toString())
		}
	}

	private fun handleException(exception: Exception) {
		handleError(exception.toString())
	}
}
